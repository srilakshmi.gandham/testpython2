# Tuples are a data type that are like lists in that they contain collections of values.
# They are unlike lists in that they're immutable which means that their contents cannot be modified.

# Tuple Forms

tuple1 = (5.6,"Harry",True,2)
tuple2 = 1,2,3,"four",5.4,False
emptyTup = ()
print(tuple1,tuple2,emptyTup)

list1 = [1,2,3,4]
tuple3 = (8,7,6)

for elements in list1:
    print(elements)
    
for items in tuple3:
    print(items)    
    
emptylist = [] 

strTup = ("Let","It","Go")
song = ""

for num in list1:
    emptylist.append(num * 2)

for word in strTup:
    song += word + " "
    
print(emptylist)    
print(song)    
    
"""
 A dictionary is a data type in Python that is like a list except that instead of having indexes such
as 0 1 2 and so forth for each value a dictionary has keys by which the values within it can be accessed.
The difference between keys in dictionaries and indexes in lists tuples and strings is that keys can be any string or any number.
"""   

dict1 = {15:"New York",25:"New Jersey",35:"Texas",45:"Dallas" }    
print(dict1[15])

Dict = {}
Dict[30]="Sheldon"
Dict[40]="Penny"
Dict[50]="Leo"
print(Dict)
print(len(Dict))
    
Dict[50]="Gru" 
print(Dict)   

dict2 = {"New York":15 ,"New Jersey":25,"Texas":35,"Dallas":45 }    
del dict2["Texas"]
print(dict2)  





