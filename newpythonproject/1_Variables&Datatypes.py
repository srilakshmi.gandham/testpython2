print("Hello World")

_myVar = 5
print(_myVar)
intVal = 10
floatVal = 2.5
boolVal = True
print(boolVal)
boolVal = False
print(intVal)
print(floatVal)
print(boolVal)

# This is a single line comment
"""
This is multiple lines comment
"""

add = 3+2
sub = 6-3
mul = 4*2
div = 6/2

print(add,sub,mul,div)

operator1 = 2
operator2 = 2
operator3 = 2

operator1 **= 2
operator2 //= 0.5
operator3 %= 1
print(operator1,operator2,operator3)

exponentiation = 3**3
print(exponentiation)

intVal = 10
intVal +=5
print(intVal)
