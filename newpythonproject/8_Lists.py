list1 = []
numlist = [1,2,3,4,5]
print(numlist[3])
numlist[3] = 20
numlist.append(25)
print(numlist)

fruits = ["apple", "orange", "banana", "pineapple"]
print(fruits[2])
print(fruits[1] + "-" + fruits[2])

sliceEx = [1,2,3,4,5,6,7]
print(sliceEx[:2])
print(sliceEx[3:6])
print(sliceEx[4:])

indexed = [0,1,0,1,1,0]
firstZero = indexed.index(0)
print(firstZero)

list1 =["Alice","Through","Looking","Glass"]
#list1.pop()
list1.pop(2)
print(list1)

list1.insert(2,"The")
print(list1)

list1.remove("Alice")
print(list1)



