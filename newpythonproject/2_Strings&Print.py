myStr1 = "Hello World!"
print(myStr1)
print('Live Long & Prosper')    
myStr3 = "Live Long & Prosper - Quote by \"Spock\" in StarTrek"
print(myStr3)

string = "example"
ex1 = string[4]
ex2 = "cape"[2]
print(ex1,ex2)

# Slicing of a String   
example = "Alligator"
exp1 = example[:5]
exp2 = example[4:7]
exp3 = example[4:]
print(exp1,exp2,exp3)

# String Methods
string = "United Nations of America"
length = len(string)
print(length)

num = 9
string = str(num)
print(string)

inttostr = 123456
print(str(inttostr)[2])

str1 ="LOWERCase"
str2 ="upperCase"
print(str1.lower(),str2.upper())

print(200)

print(str1+str2)

city ="Seattle"
state = "Washington"
print("The Seahawks are from %s, %s." %(city,state))


name = input("What city do you live in:")
print("You live in %s." %(name))

name = input("Enter your name:")
print("Your name is %s." %(name))















